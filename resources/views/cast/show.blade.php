@extends('layout.master')

@section('judul')
Detail: {{$cast->nama}}
@endsection

@section('subjudul')
{{$cast->nama}} 
@endsection

@section('content')

<h3>{{$cast->nama}} </h3>
<p>{{$cast->umur}} tahun <br><br></p> 
<p>{{$cast->bio}}</p>

@endsection