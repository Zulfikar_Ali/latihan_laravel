@extends('layout.master')

@section('judul')
Sign Up Form   
@endsection

{{-- <h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3> --}}

@section('content')
<!--form action-->
<form action="/kirim" method="post">
    @csrf
    <label for="First_name">First Name:</label>
    <br><br>
    <input type="text" name="First_name">
    <br><br>
    <label for="Last_name">Last Name:</label>
    <br><br>
    <input type="text" name="Last_name" >
    <br><br>

    <label for="">Gender:</label>
    <br><br>
    <input type="radio" name="Gender" value="0"> Male
    <br>
    <input type="radio" name="Gender" value="1"> Female
    <br> 
    <input type="radio" name="Gender" value="2"> Other
    <br><br>

    <label>Nationality:</label>
    <br><br>
    <select>
        <option value="amerika">Amerika</option>
        <option value="indonesia">Indonesia</option>
        <option value="inggris">Inggris</option>
    </select>
    <br><br>

    <label for="">Language Spoken:</label>
    <br><br>
    <input type="checkbox" name="bahasa" value="0"> Bahasa Indonesia
    <br>
    <input type="checkbox" name="bahasa" value="1"> English 
    <br> 
    <input type="checkbox" name="bahasa" value="2"> Other
    <br><br>
    
    <label>Bio:</label>
    <br><br>
    <textarea cols="30" rows="10"></textarea>
    
    <br>
    <input type="submit" value="Sign Up">
    @endsection